package perdidasporfriccion.views;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by JuanCarlos on 3/6/2015.
 */
public class VentanaMenu {
    private JButton hazenMetodoBoton;
    private JButton darcyMetodoBoton;
    private JPanel ventanaMenu;

    public static void main(String[] args) {
        JFrame frame = new JFrame("VentanaMenu");
        frame.setContentPane(new VentanaMenu().ventanaMenu);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }



    public void crearDialogoConMetodo(String metodo){
        HazenDialogo hazenDialogo = new HazenDialogo(metodo);
        hazenDialogo.pack();
        hazenDialogo.setVisible(true);
    }

    public VentanaMenu() {

        hazenMetodoBoton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                crearDialogoConMetodo("hazen");
            }
        });
        darcyMetodoBoton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

}
