package perdidasporfriccion.views;

import perdidasporfriccion.logic.DarcyPerdidasPorFriccion;
import perdidasporfriccion.logic.HazenPerdidasPorFriccion;

import javax.swing.*;
import java.awt.event.*;

public class HazenDialogo extends JDialog {
    private String metodo;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel tituloDialogoLabel;
    private JPanel formPanelHazen;
    private JLabel parametroUnoLabel;
    private JLabel parametroDosLabel;
    private JLabel parametroTresLabel;
    private JLabel parametroCuatroLabel;
    private JTextField parametroUnoTextField;
    private JTextField parametroDosTextField;
    private JTextField parametroTresTextField;
    private JTextField parametroCuatroTextField;
    private JButton calcularResultadoBoton;
    private JLabel resultadoCalculoLabel;
    private HazenPerdidasPorFriccion hazenPerdidasPorFriccion = new HazenPerdidasPorFriccion();
    private DarcyPerdidasPorFriccion darcyPerdidasPorFriccion = new DarcyPerdidasPorFriccion();

    public HazenDialogo() {
        this("Hazen");
    }

    public HazenDialogo(String metodo){

        inicializarUI();
    }

    private void inicializarUI() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        inicializarUI();
        inicializarListeners();
    }

    private void inicializarListeners() {
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

}
