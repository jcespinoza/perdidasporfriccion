package perdidasporfriccion.logic;

/**
 * Created by JuanCarlos on 3/6/2015.
 */
public class HazenPerdidasPorFriccion implements IAlgoritmoPerdidasPorFriccion{
    private double qCaudal;
    private double coeficienteMaterial;
    private double diametro;
    private double longitud;
    public final double COEFICIENTE_DE_ARRIBA = 10.643;
    public final double EXPONENTE_UNO = 1.85;
    public final double EXPONENTE_DOS = 4.87;

    public double getQCaudal() {
        return qCaudal;
    }

    public void setQCaudal(double qCaudal) {
        this.qCaudal = qCaudal;
    }

    public double getCoeficienteMaterial() {
        return coeficienteMaterial;
    }

    public void setCoeficienteMaterial(double coeficienteMaterial) {
        this.coeficienteMaterial = coeficienteMaterial;
    }

    public double getDiametro() {
        return diametro;
    }

    public void setDiametro(double diametro) {
        this.diametro = diametro;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public double calcularPerdidasPorFriccion() {
        double QConExponente = Math.pow(getQCaudal(), EXPONENTE_UNO);
        double CConExponente = Math.pow(getCoeficienteMaterial(), EXPONENTE_UNO);
        double DConExponente = Math.pow(getDiametro(), EXPONENTE_DOS);

        double dividendo = COEFICIENTE_DE_ARRIBA * QConExponente * getLongitud();
        double divisor = CConExponente * DConExponente; //Posible division entre cero

        double calculoPerdidaPorFriccion = dividendo / divisor;

        return calculoPerdidaPorFriccion;
    }
}
