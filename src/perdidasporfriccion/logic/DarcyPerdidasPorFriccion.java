package perdidasporfriccion.logic;

/**
 * Created by JuanCarlos on 3/6/2015.
 */
public class DarcyPerdidasPorFriccion implements IAlgoritmoPerdidasPorFriccion {

    private double FactorDeFriccion;
    private double velocidad;
    private double diametro;
    private double longitud;
    public final double gravedadEnMetrosPorSegundoCuadrado = 9.8;

    public double getFactorDeFriccion() {
        return FactorDeFriccion;
    }

    public void setFactorDeFriccion(double factorDeFriccion) {
        FactorDeFriccion = factorDeFriccion;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getDiametro() {
        return diametro;
    }

    public void setDiametro(double diametro) {
        this.diametro = diametro;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public double calcularPerdidasPorFriccion() {
        double VConExponente = Math.pow(getVelocidad(), 2);
        double dobleGravedad = gravedadEnMetrosPorSegundoCuadrado * 2;

        double dividendo = getFactorDeFriccion() * getLongitud() * VConExponente;
        double divisor = getDiametro()*dobleGravedad; //Posible division entre cero si Diametro no fue asignado

        double calculoPerdidasPorFriccion = dividendo / divisor;

        return calculoPerdidasPorFriccion;
    }
}
