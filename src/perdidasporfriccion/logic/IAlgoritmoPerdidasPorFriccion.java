package perdidasporfriccion.logic;

/**
 * Created by JuanCarlos on 3/6/2015.
 */
public interface IAlgoritmoPerdidasPorFriccion {
    double calcularPerdidasPorFriccion();
}
